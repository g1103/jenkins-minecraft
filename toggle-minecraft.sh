#!/bin/bash
#
# starting and stopping minecraft server
# this script is calling a "go" startup script, the contents of that are
# commented out at the bottom of this script
#
# version 1

EXECDIR="~/mc/1122standard"


pidcheck() {
pid=`ps -ef | grep -v grep | grep forge-custom.jar | awk '{print $2}'`
if [[ $pid ]]
then
  running="yes"
else
  running="no"
fi
}


pidcheck
if [[ $running == "yes" ]]
then
  printf "\n\nServer is running so shutting it down"
  kill $pid
  sleep 10
  pidcheck
  if [[ $pid ]]
  then
    printf "\n\nUnable to kill server"
    exit 1
  else
    printf "\n\nSuccess, server killed"
    exit 0
  fi
elif [[ $running == "no" ]]
then
  printf "\n\nServer not running, starting it\n\n"
  cd $EXECDIR
  screen -d -m ./go &
  printf "\n\n"  
  sleep 5
  pidcheck
  if [[ $pid ]]
  then
     printf "\n\nServer sucessfully started, $pid\n\n"
  else
     printf "\n\nFailed to start server\n\n"
     exit 1
  fi
else
  printf "\n\nUnknown Error\n\n"
fi

####
# This is the "go" script it's calling, just starts up minecraft with some java options (using forge)
#
#!/bin/bash
#
#java -Xms4096M -Xmx4096M -Djava.net.preferIPv4Stack=true -XX:UseSSE=3 -XX:-DisableExplicitGC -XX:+UseParallelOldGC -XX:ParallelGCThreads=4 -XX:+AggressiveOpts -jar jar/forge-custom.jar nogui
#  
